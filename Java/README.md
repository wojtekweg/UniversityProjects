# Programs list

**1. DNA sorter**
  - Given the DNA genome, program validates the input and sort the codons in lexicographical order, using the quicksort algorithm. Genome is the sequence of the A, C, T and G letters and codone is the three letters long sequence
  
**2. Recursive and iterative search** 
  - Finding the leftmost and rightmost element. Both iterative and recursive algorithms are included
  
**3. ONP <-> INF converter**
  - Validate the ONP or INF formula and convert it INF to ONP or ONP to INF
