import java.util.Scanner;

class table {
    int[][] board;
    int max;
    int min;
    int height;
    int width;
    Stack values;

    table (int[][] boardIn, int heightIn, int widthIn)
    {
        this.board = boardIn;
        this.height = heightIn;
        this.width = widthIn;
        this.max = boardIn[widthIn-1][heightIn-1];
        this.min = boardIn[0][0];
        this.values = new Stack(heightIn*widthIn);
    }
    public void Push (String thing) {
        this.values.Push(thing);
    }
}
class Stack {
    int maxSize;
    String[] Elem;
    int top;

    public Stack (int Size) {
        maxSize = Size;
        Elem = new String[maxSize];
        top = maxSize;
    }
    boolean isEmpty (){
        return top == maxSize;
    }
    String Pop() {
        if(top<maxSize)
            return Elem[top++];
        else
            return "";
    }
    String Peek() {
        if(top<maxSize)
            return Elem[top];
        else
            return "";
    }
    void Push(String x) {
        if(top>0){
            top--;
            Elem[top]=x;
        }
    }
}

public class Source {
    public static Scanner input = new Scanner(System.in);

    public static String recursiveFirst(table input, int search, int i, int L, int R, int outputW, int outputH) {
        int locWidth = input.width;
        int locHeight = input.height ;

        if (i < locWidth) {
            if (L < R) {
                int m = ((L + R) / 2);
                if (input.board[i][m] < search)
                    L = m + 1;
                else
                    R = m;
                return recursiveFirst(input, search, i, L, R, outputW, outputH);
            }
            //checking if index is correct
            if (i >= 0 && L >= 0 && i < locWidth && L < locHeight)
            {
                //making sure thats the same value
                if ((input.board[i][L] == search)) {
                    //if better row
                    if ((i < outputH)||(outputH==-1 && outputW == -1))
                    {
                        outputW = L;
                        outputH = i;
                    }
                    else if ((i <= outputH)&&(L < outputW))
                    {
                        outputW = L;
                        outputH = i;
                    }
                }
            }
            i++;
            return recursiveFirst(input, search, i, 0, input.height, outputW, outputH);
        }
        if (outputH >= 0 && outputW >= 0)
            input.Push("(" + outputH + "," + outputW + ")");
        else input.Push("nie ma " + search);
        return ("(" + outputH + "," + outputW + ")");
    }

    public static String recursiveLast(table input, int search, int i, int L, int R, int outputW, int outputH) {
        int locWidth = input.width;
        int locHeight = input.height ;
        if (i < locWidth) {
            if (L < R) {
                int m = ((L + R) / 2);
                if (input.board[i][m] > search)
                    R = m;
                else
                    L = m + 1;
                return recursiveLast(input, search, i, L, R, outputW, outputH);
            }
            if ((i > outputH)&&(R-1 < locHeight)&&(R-1 >= 0)) {
                if ((input.board[i][R-1] == search)) {
                    outputW = R-1;
                    outputH = i;
                }
            }
            else if ((R-1 >= outputW)&&(R-1 < locHeight)&&(R-1 >= 0)) {
                if ((input.board[i][R-1] == search)) {
                    outputW = R-1;
                    outputH = i;
                }
            }
            i++;
            return recursiveLast(input, search, i, 0, locHeight, outputW, outputH);
        }
        if (outputH >= 0 && outputW >= 0)
            input.Push("(" + outputH + "," + outputW + ")");
        else input.Push("nie ma " + search);
        return ("(" + outputH + "," + outputW + ")");
    }

    public static String iterativeFirst (table input, int search) {
        int locWidth = input.width;
        int locHeight = input.height ;
        //for every row:
        int outputW = -1;
        int outputH = -1;
        for (int i = 0; i < locWidth; i++) {
            int L = 0;
            int R = locHeight;
            while (L < R) {
                int m = ((L + R) / 2);
                if (input.board[i][m] < search)
                    L = m + 1;
                else
                    R = m;
            }

            //checking if index is correct
            if (i >= 0 && L >= 0 && i < locWidth && L < locHeight)
            {
                //making sure thats the same value
                if ((input.board[i][L] == search)) {
                    //if better row
                    if ((i < outputH)||(outputH==-1 && outputW == -1))
                    {
                        outputW = L;
                        outputH = i;
                    }
                    else if ((i <= outputH)&&(L < outputW))
                    {
                        outputW = L;
                        outputH = i;
                    }
                }
            }
        }
        if (outputH >= 0 && outputW >= 0)
            return ("(" + outputH + "," + outputW + ")");
        else return "nie ma " + search;
    }

    public static String iterativeLast (table input, int search) {
        int locWidth = input.width;
        int locHeight = input.height ;
        //for every row:
        int outputW = -1;
        int outputH = -1;
        for (int i = 0; i < locWidth; i++) {
            int L = 0;
            int R = locHeight;
            while (L < R) {
                int m = ((L + R) / 2);
                if (input.board[i][m] > search)
                    R = m;
                else
                    L = m + 1;
            }

            if ((i > outputH)&&(R-1 < locHeight)&&(R-1 >= 0)) {
                if ((input.board[i][R-1] == search)) {
                    outputW = R-1;
                    outputH = i;
                }
            }
            else if ((R-1 >= outputW)&&(R-1 < locHeight)&&(R-1 >= 0)) {
                if ((input.board[i][R-1] == search)) {
                    outputW = R-1;
                    outputH = i;
                }
            }
        }
        if (outputH >= 0 && outputW >= 0)
            return ("(" + outputH + "," + outputW + ")");
        else return "nie ma " + search;
    }

    public static void main ( String [] args ) {
        int records = input.nextInt();
        while (records > 0) {
            int nSize = input.nextInt();
            int mSize = input.nextInt();

            //max width = 100
            //max height = 100
            //element range = -2^15 - 2^15
            int[][] table = new int[nSize][mSize];
            for (int w = 0; w < nSize; w++)
            {
                for (int h = 0; h < mSize; h++)
                {
                    int in = input.nextInt();
                    table[w][h] = in;
                }
            }
            int min = table[0][0];
            int max = table[nSize-1][mSize-1];
            table temp = new table(table, mSize, nSize);
            int Search = input.nextInt();
            if (Search > min && Search < max)
            {
                //System.out.println("RekPier: " + Search + " w " + recursiveFirst(temp, Search, 0, 0,  temp.height, 0, 0));
                    System.out.print("RekPier: " + Search + " w ");
                    recursiveFirst(temp, Search, 0, 0,  temp.height, -1, -1);
                    System.out.println(temp.values.Pop());
                //System.out.println("RekOst: " + Search + " w " + recursiveLast(temp, Search, 0, 0, temp.height, 0, 0));
                    System.out.print("RekOst: " + Search + " w ");
                    recursiveLast(temp, Search, 0, 0, temp.height, -1, -1);
                    System.out.println(temp.values.Pop());
                System.out.println("IterPier: " + Search + " w " + iterativeFirst(temp, Search));
                System.out.println("IterOst: " + Search + " w " + iterativeLast(temp, Search));
            }
            else if ((Search == min)&&(Search != max))
            {
                System.out.println("RekPier: " + Search + " w (" + 0 + "," + 0 + ")");
                    System.out.print("RekOst: " + Search + " w ");
                    recursiveLast(temp, Search, 0, 0, temp.height, -1, -1);
                    System.out.println(temp.values.Pop());
                System.out.println("IterPier: " + Search + " w (" + 0 + "," + 0 + ")");
                System.out.println("IterOst: " + Search + " w " + iterativeLast(temp, Search));
            }
            else if ((Search == max)&&(Search != min))
            {
                //System.out.println("RekPier: " + Search + " w " + recursiveFirst(temp, Search, 0, 0, temp.height, 0, 0));
                    System.out.print("RekPier: " + Search + " w ");
                    recursiveFirst(temp, Search, 0, 0,  temp.height, -1, -1);
                    System.out.println(temp.values.Pop());
                System.out.println("RekOst: " + Search + " w (" + (nSize-1) + "," + (mSize-1) + ")");
                System.out.println("IterPier: " + Search + " w " + iterativeFirst(temp, Search));
                System.out.println("IterOst: " + Search + " w (" + (nSize-1) + "," + (mSize-1) + ")");
            }
            else if (Search == min)
            {
                System.out.println("RekPier: " + Search + " w (" + 0 + "," + 0 + ")");
                System.out.println("RekOst: " + Search + " w (" + (nSize-1) + "," + (mSize-1) + ")");
                System.out.println("IterPier: " + Search + " w (" + 0 + "," + 0 + ")");
                System.out.println("IterOst: " + Search + " w (" + (nSize-1) + "," + (mSize-1) + ")");
            }
            else {
                System.out.println("RekPier: nie ma " + Search);
                System.out.println("RekOst: nie ma " + Search);
                System.out.println("IterPier: nie ma " + Search);
                System.out.println("IterOst: nie ma " + Search);
            }
            System.out.println("---");
            records--;
        }
    }
}

