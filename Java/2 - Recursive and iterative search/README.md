# Recursive and iterative search

## Task

Write both recursive and iterative functions for printing the first and the last occurence of the given element in the given array.

Complexity of the solution has to be lower than O(max(n,m))^2, where n and m are the height and width of the array.
