# Hello world!
Here you can find some simple projects, mainly in C++ and Java, I made during my first University year. 
These are my first programs that I have written in my life. 
Every project has it's own "read me" file and sample tests, to show how it works. 

## What can you find here?
### Java
If you look after simple projects, based on the popular algorithms (mainly sorting and searching), then you should check out the codes written in Java. 

### C/C++
On the other hand, C++ (and one in C) projects rely on basic programming issues, like dynamic allocated memory, pointers or bitwise operations. 
