//Wojciech Wegrzyn, 2020
//Simulation of 2D board, with variable number of columns in each row, based on C malloc() function
//Program was written in C language based on '99 compiler, because of the University requirements  
#include <stdio.h>
#include <stdlib.h>

int compare(char *cmd, char a, char b, char c);
void ALR (int** TABLE, int* HEIGHT, int* WIDTH, int w);
void AFR (int** TABLE, int* HEIGHT, int* WIDTH);
void IBR (int** TABLE, int* HEIGHT, int* WIDTH, int r);
void IAR (int** TABLE, int* HEIGHT, int* WIDTH, int r);
void SWR (int** TABLE, int* HEIGHT, int* WIDTH);
void DFR (int** TABLE, int* HEIGHT, int* WIDTH);
void RMR (int** TABLE, int* HEIGHT, int* WIDTH, int r);
void DFC(int **pInt, int *pInt1, int *pInt2, int h_it, int curr_width);

int main() {
    int test = 1; //true/false value - true for more detailed printing

    //2d array declaration
    int HEIGHT_MAX = 1;
    int WIDTH_MAX = 1;
    int **TABLE = malloc(HEIGHT_MAX * sizeof(int*));

    //declaration of the input-reading array
    char *cmd = malloc(sizeof(char) * 4);

    while (HEIGHT_MAX) {
        scanf("%3s", cmd);

        //20. command for ending the program
		//which frees the allocated memory
        if (compare(cmd, 'E', 'N', 'D'))
        {
            fflush(stdin);
            free(cmd);
            int i;
            for (i=0; i < HEIGHT_MAX - 1; i++)
            {
                free(*(TABLE + i));
            }
            free(TABLE);
            return 0;
        }
        //19. command for printing an array
        else if (compare(cmd, 'P', 'R', 'T'))
        {
            int y, x;
            for (y = 0; y < HEIGHT_MAX-1; y++) {
                if (test) printf("%i: ", y);
                if (test) printf("[%i] ", *(*(TABLE + y)));
                if (*(*(TABLE + y)) > 0)
                {
                    for (x = 1; x <= *(*(TABLE + y)); x++) {
                        printf("%i ", *(*(TABLE + y) + x));
                    }
                    if (!test) printf("\n");
                }
                if (test) printf("\n");
            }
        }
        //01. command to add the new row at the beginning
        else if (compare(cmd, 'A', 'F', 'R'))
        {
            AFR(TABLE, &HEIGHT_MAX, &WIDTH_MAX);
            TABLE = realloc(TABLE, HEIGHT_MAX * sizeof(int *));
        }
        //02. command to add the new row at the end
        else if (compare(cmd, 'A', 'L', 'R'))
        {
            int w = 0; //width of the row to insert
            scanf("%i", &w);
            ALR(TABLE, &HEIGHT_MAX, &WIDTH_MAX, w);
            TABLE = realloc(TABLE, HEIGHT_MAX * sizeof(int *));
        }
        //03. command to add the new column at the beginning
        else if (compare(cmd, 'A', 'F', 'C'))
        {
            int h = 0; //height of the column to insert
            scanf("%i", &h);
            int h_it = 0;

            //adding the columns to existing rows
            while ((h_it < HEIGHT_MAX - 1)&&(h_it < h)) {
                WIDTH_MAX = WIDTH_MAX + 1;

                //increasing the row capacity by one
                *(TABLE + h_it) = realloc(*(TABLE + h_it), WIDTH_MAX * sizeof(int));

                //increase the first row value by one
                *(*(TABLE + h_it)) += 1;

                //move all the row values further, except the first and second
                int i = 0;
                for (i = *(*(TABLE + h_it)) - 1; i > 0; i--)
                {
                    *(*(TABLE + h_it) + i + 1) = *(*(TABLE + h_it) + i);
                }

                //read the added value
                int e = 0;
                scanf("%i", &e);

                //add the read value
                *(*(TABLE + h_it) + 1) = e;

                h_it++;
            }
            //make a new rows with only one given value
            while (h_it < h)
            {
                //making the width = 1
                int w = 1;
                if (w + 1 > WIDTH_MAX)
                    WIDTH_MAX = w + 1;

                //creating new row
                int *ROW = (int *) malloc((w + 1) * sizeof(int));

                //read the added value
                int e = 0;
                scanf("%i", &e);

                //assigning the values
                *ROW = w;
                *(ROW + 1) = e;

                //adding the row
                *(TABLE + HEIGHT_MAX - 1) = ROW;

                //increasing height of table by one
                HEIGHT_MAX = HEIGHT_MAX + 1;
                TABLE = realloc(TABLE, HEIGHT_MAX * sizeof(int*));

                h_it++;
            }
        }
        //04. command to add the new column at the end
        else if (compare(cmd, 'A', 'L', 'C'))
        {
            int h = 0; //height of the column to insert
            scanf("%i", &h);
            int h_it = 0;

            //adding the columns to existing rows
            while ((h_it < HEIGHT_MAX - 1)&&(h_it < h)) {
                WIDTH_MAX = WIDTH_MAX + 1;

                //increasing the row capacity by one
                *(TABLE + h_it) = (int*) realloc(*(TABLE + h_it), WIDTH_MAX * sizeof(int));

                //increase the first row value by one
                *(*(TABLE + h_it)) += 1;

                //read the added value
                int e = 0;
                scanf("%i", &e);

                //add the read value
                *(*(TABLE + h_it) + *(*(TABLE + h_it))) = e;

                h_it++;
            }
            //make a new rows with only one given value
            while (h_it < h)
            {
                //making the width = 1
                int w = 1;
                if (w + 1 > WIDTH_MAX)
                    WIDTH_MAX = w + 1;

                //creating new row
                int *ROW = (int *) malloc((w + 1) * sizeof(int));

                //read the added value
                int e = 0;
                scanf("%i", &e);

                //assigning the values
                *ROW = w;
                *(ROW + 1) = e;

                //adding the row
                *(TABLE + HEIGHT_MAX - 1) = ROW;

                //increasing height of table by one
                HEIGHT_MAX = HEIGHT_MAX + 1;
                TABLE = realloc(TABLE, HEIGHT_MAX * sizeof(int*));

                h_it++;
            }
        }
        //05. command to add the new row before the given place
        else if (compare(cmd, 'I', 'B', 'R'))
        {
            int r = 0; //height of the row, before which the new one will be inserted
            scanf("%i", &r);

			//adding the row at the beginning
            if (r == 0) {
                AFR(TABLE, &HEIGHT_MAX, &WIDTH_MAX);
                TABLE = realloc(TABLE, HEIGHT_MAX * sizeof(int*));
            }
			//adding the row in the middle
            else if (r < HEIGHT_MAX - 1) {
                IBR(TABLE, &HEIGHT_MAX, &WIDTH_MAX, r);
                TABLE = realloc(TABLE, HEIGHT_MAX * sizeof(int*));
            }
			//invalid input
            else {
                //empty input scan
                int w = 0;
                scanf("%i", &w);
                int i = 0;
                for (i = 0; i < w; i++) {
                        scanf("%i", &w);
                }
            }
        }
        //06. command to add the new row after the given place
        else if (compare(cmd, 'I', 'A', 'R'))
        {
            int r = 0; //height of the row, after which the new one will be inserted
            scanf("%i", &r);

			//adding the row at the end
            if (r == HEIGHT_MAX) {
                int w = 0;
                scanf("%i", &w);

                ALR(TABLE, &HEIGHT_MAX, &WIDTH_MAX, w);
                TABLE = realloc(TABLE, HEIGHT_MAX * sizeof(int *));
            }
			//adding the row in the middle
            else if (r < HEIGHT_MAX - 1) {
                IAR(TABLE, &HEIGHT_MAX, &WIDTH_MAX, r);
                TABLE = realloc(TABLE, HEIGHT_MAX * sizeof(int*));
            }
			//invalid input
            else {
                //empty input scan
                int w = 0;
                scanf("%i", &w);
                int i = 0;
                for (i = 0; i < w; i++) {
                    scanf("%i", &w);
                }
            }
        }
        //07. command to add the new column before the given place
        else if (compare(cmd, 'I', 'B', 'C'))
        {
            //before which column do we want to add new one
            int c = 0;
            scanf("%i", &c);

            //making the c count from 1
            c++;

            //for how many rows we are going to add
            int h = 0;
            scanf("%i", &h);
            int h_it = 0;

            //adding the columns to existing rows
            while ((h_it < HEIGHT_MAX - 1)&&(h_it < h)) {
                WIDTH_MAX = WIDTH_MAX + 1;

                //read the added value
                int e = 0;
                scanf("%i", &e);

                //checking is the column before we are going to add exists
                if (*(*(TABLE + h_it)) > c - 1) {
                    //increasing the row capacity by one
                    *(TABLE + h_it) = (int *) realloc(*(TABLE + h_it), WIDTH_MAX * sizeof(int));

                    //increase the first row value by one
                    *(*(TABLE + h_it)) += 1;

                    //move the values in row starting of the c-1 value
                    int i = 0;
                    for (i = *(*(TABLE + h_it)) - 1; i > c - 1; i--)
                    {
                        *(*(TABLE + h_it) + i + 1) = *(*(TABLE + h_it) + i);
                    }

                    //add the read value
                    *(*(TABLE + h_it) + c) = e;
                }
                //adding the value at the end of row
                else
                {
                    //increasing the row capacity by one
                    *(TABLE + h_it) = (int*) realloc(*(TABLE + h_it), WIDTH_MAX * sizeof(int));

                    //increase the first row value by one
                    *(*(TABLE + h_it)) += 1;

                    //add the read value
                    *(*(TABLE + h_it) + *(*(TABLE + h_it))) = e;
                }
                h_it++;
            }
            //make a new rows with only one given value
            while (h_it < h)
            {
                //making the width = 1
                int w = 1;
                if (w + 1 > WIDTH_MAX)
                    WIDTH_MAX = w + 1;

                //creating new row
                int *ROW = (int *) malloc((w + 1) * sizeof(int));

                //read the added value
                int e = 0;
                scanf("%i", &e);

                //assigning the values
                *ROW = w;
                *(ROW + 1) = e;

                //adding the row
                *(TABLE + HEIGHT_MAX - 1) = ROW;

                //increasing height of table by one
                HEIGHT_MAX = HEIGHT_MAX + 1;
                TABLE = realloc(TABLE, HEIGHT_MAX * sizeof(int*));

                h_it++;
            }
        }
        //08. command to add the new column after the given place
        else if (compare(cmd, 'I', 'A', 'C'))
        {
            //before which column do we want to add new one
            int c = 0;
            scanf("%i", &c);

            //making the c count from 1
            c++;

            //for how many rows we are going to add
            int h = 0;
            scanf("%i", &h);
            int h_it = 0;

            //adding the columns to existing rows
            while ((h_it < HEIGHT_MAX - 1)&&(h_it < h)) {
                WIDTH_MAX = WIDTH_MAX + 1;

                //read the added value
                int e = 0;
                scanf("%i", &e);

                //checking is the column before we are going to add exists
                if (*(*(TABLE + h_it)) > c) {
                    //increasing the row capacity by one
                    *(TABLE + h_it) = realloc(*(TABLE + h_it), WIDTH_MAX * sizeof(int));

                    //increase the first row value by one
                    *(*(TABLE + h_it)) += 1;

                    //move the values in row starting of the c-1 value
                    int i = 0;
                    for (i = *(*(TABLE + h_it)) - 1; i > c - 1; i--)
                    {
                        *(*(TABLE + h_it) + i + 1) = *(*(TABLE + h_it) + i);
                    }

                    //add the read value
                    *(*(TABLE + h_it) + c + 1) = e;

                }
                //adding the value at the end of row
                else
                {
                    //increasing the row capacity by one
                    *(TABLE + h_it) = (int*) realloc(*(TABLE + h_it), WIDTH_MAX * sizeof(int));

                    //increase the first row value by one
                    *(*(TABLE + h_it)) += 1;

                    //add the read value
                    *(*(TABLE + h_it) + *(*(TABLE + h_it))) = e;
                }
                h_it++;
            }
            //make a new rows with only one given value
            while (h_it < h)
            {
                //making the width = 1
                int w = 1;
                if (w + 1 > WIDTH_MAX)
                    WIDTH_MAX = w + 1;

                //creating new row
                int *ROW = (int *) malloc((w + 1) * sizeof(int));

                //read the added value
                int e = 0;
                scanf("%i", &e);

                //assigning the values
                *ROW = w;
                *(ROW + 1) = e;

                //adding the row
                *(TABLE + HEIGHT_MAX - 1) = ROW;

                //increasing height of table by one
                HEIGHT_MAX = HEIGHT_MAX + 1;
                TABLE = realloc(TABLE, HEIGHT_MAX * sizeof(int*));

                h_it++;
            }
        }
        //09. command to swap two rows
        else if (compare(cmd, 'S', 'W', 'R')) {
            SWR(TABLE, &HEIGHT_MAX, &WIDTH_MAX);
        }
        //10. command to swap two columns
        else if (compare(cmd, 'S', 'W', 'C')) {

			//reading the coordinates of the columns to swap
            int c = 0;
            scanf("%i", &c);

            int d = 0;
            scanf("%i", &d);

            //making the c and d count from 1
            c++;
            d++;

            //for every row that we have
            int h_it = 0;
            while (h_it < HEIGHT_MAX - 1)
            {
                //current row width
                int curr_width = *(*(TABLE + h_it));

                //if the row has that columns
                if ((c < curr_width + 1)&&(d < curr_width + 1))
                {
                    //swap
                    int temp = *(*(TABLE + h_it) + c);
                    *(*(TABLE + h_it) + c) = *(*(TABLE + h_it) + d);
                    *(*(TABLE + h_it) + d) = temp;
                }
                h_it++;
            }
        }
        //11. command to delete the first row
        else if (compare(cmd, 'D', 'F', 'R')) {
            DFR(TABLE, &HEIGHT_MAX, &WIDTH_MAX);
            TABLE = realloc(TABLE, HEIGHT_MAX * sizeof(int *));
        }
        //12. command to delete the last row
        else if (compare(cmd, 'D', 'L', 'R')) {
            if (HEIGHT_MAX - 1 > 0) {
                *(*(TABLE + HEIGHT_MAX - 2)) = 0; //making last row equal zero
                HEIGHT_MAX--; //reducing maximum height
                TABLE = realloc(TABLE, HEIGHT_MAX * sizeof(int *));
            }
        }
        //13. command to delete the first column
        else if (compare(cmd, 'D', 'F', 'C')) {
            //for every row that we have
            int h_it = 0;
            for (h_it = 0; h_it < HEIGHT_MAX - 1; h_it++)
            {
                //current row width
                int curr_width = *(*(TABLE + h_it));

				//normal deletion
                if (curr_width > 1)
                    DFC(TABLE, &HEIGHT_MAX, &WIDTH_MAX, h_it, curr_width);

				//deleting the whole row, because it's empty
                else
                {
					//moving the rows above
                    int i;
                    for (i = h_it; i + 1 < HEIGHT_MAX; i++)
                    {
                        *(TABLE + i) = *(TABLE + i + 1);
                    }

					//resizing the array
                    HEIGHT_MAX = HEIGHT_MAX - 1;
                    TABLE = realloc(TABLE, HEIGHT_MAX * sizeof(int *));
                    h_it--;
                }
            }
        }
        //14. command to delete the last column
        else if (compare(cmd, 'D', 'L', 'C')) {
            //for every row that we have
            int h_it = 0;
            while (h_it < HEIGHT_MAX - 1)
            {
                //current row width
                int curr_width = *(*(TABLE + h_it));

                //if the row has at least one column
                if (curr_width > 1)
                {
                    //make the column width smaller
                    *(*(TABLE + h_it)) = *(*(TABLE + h_it)) - 1;
                }
                //delete the row
                else
                {
					//moving the rows above
                    int i;
                    for (i = h_it; i + 1 < HEIGHT_MAX; i++)
                    {
                        *(TABLE + i) = *(TABLE + i + 1);
                    }

					//resizing the array
                    HEIGHT_MAX = HEIGHT_MAX - 1;
                    TABLE = realloc(TABLE, HEIGHT_MAX * sizeof(int *));
                    h_it--;
                }
                h_it++;
            }
        }
        //15. command to delete the given row
        else if (compare(cmd, 'R', 'M', 'R')) {
            int r = 0; //reading the row, which will be deleted
            scanf("%i", &r);
            RMR(TABLE, &HEIGHT_MAX, &WIDTH_MAX, r);
            TABLE = realloc(TABLE, HEIGHT_MAX * sizeof(int *));
        }
        //16. command to delete the given column
        else if (compare(cmd, 'R', 'M', 'C')) {
            //this command is copied from RMB command
            int row_st, col_st, height, width;
            scanf("%i", &col_st);

            row_st = 0;
            height = HEIGHT_MAX;
            width = 1;

            //for the every of read rows
            int row_it;
            int deleted_rows = 0;
            for (row_it = row_st; row_it < height + row_st - deleted_rows; row_it++)
            {
                //validate the row index - operation take place only insade a table
                if (row_it < HEIGHT_MAX - 1)
                {
                    //read the width of the current row that we look into
                    int curr_width = *(*(TABLE + row_it));

                    //if we know that there is whole row to delete
                    if ((width > curr_width)&&(col_st == 0)) {
                        //remove row
                        if (row_it == 0)
                            DFR(TABLE, &HEIGHT_MAX, &WIDTH_MAX);
                        else
                            RMR(TABLE, &HEIGHT_MAX, &WIDTH_MAX, row_it);
                        TABLE = realloc(TABLE, HEIGHT_MAX * sizeof(int *));

                        //we deleted the row so we reset the iterator
                        row_it--;
                        deleted_rows++;
                    }

                    //we are removing the thing inside of the column
                    else {
                        int curr_col;
                        for (curr_col = col_st; curr_col < col_st + width; curr_col++)
                        {
                            //move every column from the end one step before, until we reach the c column
                            int i;
                            for (i = col_st + 1; i < curr_width; i++)
                            {
                                *(*(TABLE + row_it) + i) = *(*(TABLE + row_it) + i + 1);
                            }

                            //make the column width smaller
                            //we make it only if the col start index is inside the row
                            if (col_st < curr_width) *(*(TABLE + row_it)) = *(*(TABLE + row_it)) - 1;
                        }
                    }
                }
            }

        }
        //17. command to insert the table
        else if (compare(cmd, 'I', 'S', 'B')) {
            int row_st, col_st, height, width;
            scanf("%i", &row_st);
            scanf("%i", &col_st);
            scanf("%i", &height);
            scanf("%i", &width);

            int h_it = 0;
            int w_it = 0;
            //for every row, starting from read
            for (h_it = row_st; h_it < row_st + height; h_it++)
            {
                //create a new row
                if (h_it > HEIGHT_MAX - 2)
                {
                    //creating a new row with with values, read inside function
                    ALR(TABLE, &HEIGHT_MAX, &WIDTH_MAX, width);
                    TABLE = realloc(TABLE, HEIGHT_MAX * sizeof(int *));
                }
                //inserting values
                else
                {
                    for (w_it = col_st; w_it < col_st + width; w_it++)
                    {
                        int curr_width = *(*(TABLE + h_it));
                        if (w_it < curr_width)
                        {
                            WIDTH_MAX = WIDTH_MAX + 1;

                            //read the added value
                            int e = 0;
                            scanf("%i", &e);

                            //increasing the row capacity by one
                            *(TABLE + h_it) = (int *) realloc(*(TABLE + h_it), WIDTH_MAX * sizeof(int));

                            //increase the first row value by one
                            *(*(TABLE + h_it)) += 1;

                            //move the values in row starting of the c-1 value
                            int i = 0;
                            for (i = *(*(TABLE + h_it)) - 1; i > w_it - 1; i--)
                            {
                                *(*(TABLE + h_it) + i + 1) = *(*(TABLE + h_it) + i);
                            }

                            //add the read value
                            *(*(TABLE + h_it) + w_it + 1) = e;
                        }
                        else
                        {
                            //we append at the end
                            WIDTH_MAX = WIDTH_MAX + 1;

                            //increasing the row capacity by one
                            *(TABLE + h_it) = (int*) realloc(*(TABLE + h_it), WIDTH_MAX * sizeof(int));

                            //increase the first row value by one
                            *(*(TABLE + h_it)) += 1;

                            //read the added value
                            int e = 0;
                            scanf("%i", &e);

                            //add the read value
                            *(*(TABLE + h_it) + *(*(TABLE + h_it))) = e;
                        }
                    }
                }
            }

        }
        //18. command to remove the sub-table
        else if (compare(cmd, 'R', 'M', 'B')) {
            int row_st, col_st, height, width;
            scanf("%i", &row_st); //STARTING ROW
            scanf("%i", &height); //FOR HOW MANY ROWS
            scanf("%i", &col_st); //STARTING COLUMN
            scanf("%i", &width); //FOR HOW MANY COLUMNS

            //for the every of read rows
            int row_it;
            int deleted_rows = 0;
            for (row_it = row_st; row_it < height + row_st - deleted_rows; row_it++)
            {
                //validate the row index - operation take place only insade a table
                if (row_it < HEIGHT_MAX - 1)
                {
                    //read the width of the current row that we look into
                    int curr_width = *(*(TABLE + row_it));

                    //if we know that there is whole row to delete
                    if ((width >= curr_width)&&(col_st == 0)) {
                        //remove row
                        if (row_it == 0)
                            DFR(TABLE, &HEIGHT_MAX, &WIDTH_MAX);
                        else
                            RMR(TABLE, &HEIGHT_MAX, &WIDTH_MAX, row_it);
                        TABLE = realloc(TABLE, HEIGHT_MAX * sizeof(int *));

                        //we deleted the row so we reset the iterator
                        row_it--;
                        deleted_rows++;
                    }

                    //we are removing the thing inside of the column
                    else {
                        int curr_col;
                        for (curr_col = col_st; curr_col < col_st + width; curr_col++)
                        {
                            if (curr_col < curr_width) {
                                //move every column from the end one step before, until we reach the c column
                                int i;
                                int condition = 0;
                                for (i = col_st + 1; i < curr_width; i++) {
                                    *(*(TABLE + row_it) + i) = *(*(TABLE + row_it) + i + 1);
                                    condition = 1;
                                }

                                //make the column width smaller
                                //if ((col_st < curr_width) && condition)
                                *(*(TABLE + row_it)) = *(*(TABLE + row_it)) - 1;
                            }
                        }
                    }
                }
            }
        }
		//invalid input flush
        else {
            fflush(stdin);
            continue;
        }
    }
}

//read the input command
int compare(char *cmd, char a, char b, char c) {
    if (*(cmd) != a)
        return 0;
    if (*(cmd + 1) != b)
        return 0;
    if (*(cmd + 2) != c)
        return 0;
    return 1;
}

//adding the row at beginning of the table
void AFR (int** TABLE, int* HEIGHT, int* WIDTH) {
    int w = 0; //read the width of the row
    scanf("%i", &w);
    if (w > 0) {
		//resizing the maximum width
        if (w + 1 > *WIDTH)
            *WIDTH = w + 1;

		//declaring the row array, with one more element for the width size
        int *ROW = (int *) malloc((w + 1) * sizeof(int));

		//reading the input and writing in into the array
        int x, c;
        *ROW = w;
        for (x = 1; x < *WIDTH + 1; x++) {
            if (x < w + 1) {
                scanf("%i", &c);
                *(ROW + x) = c;
            }
        }

		//adding the row at the beggining of 2D table
        int i = 0;
        for (i = *HEIGHT-1; i > 0; i--)
            *(TABLE + i) = *(TABLE + i - 1);
        *TABLE = ROW;
        *HEIGHT = *HEIGHT + 1;
    }
}

//adding the row at the end of the table
void ALR (int** TABLE, int* HEIGHT, int* WIDTH, int w) {
    if (w > 0) {
		//resizing the maximum width
        if (w + 1 > *WIDTH)
            *WIDTH = w + 1;

		//declaring the row array, with one more element for the width size
        int *ROW = (int *) malloc((w + 1) * sizeof(int));

		//reading the input and writing in into the array
        int x, c;
        *ROW = w;
        for (x = 1; x < *WIDTH + 1; x++) {
            if (x < w + 1) {
                scanf("%i", &c);
                *(ROW + x) = c;
            }
        }

		//adding the row at the end of 2D table
        *(TABLE + *HEIGHT - 1) = ROW;
        *HEIGHT = *HEIGHT + 1;
    }
}

//inserting the row before the existing row
void IBR (int** TABLE, int* HEIGHT, int* WIDTH, int r) {
    int w = 0; //reading the width of the row
    scanf("%i", &w);
    if (w > 0) {
		//resizing the maximum width
        if (w + 1 > *WIDTH)
            *WIDTH = w + 1;

		//declaring the row array, with one more element for the width size
        int *ROW = (int *) malloc((w + 1) * sizeof(int));

		//reading the input and writing in into the array
        int x, c;
        *ROW = w;
        for (x = 1; x < *WIDTH + 1; x++) {
            if (x < w + 1) {
                scanf("%i", &c);
                *(ROW + x) = c;
            }
        }

		//moving the rows above the row index that we want to insert a new one
        int i = 0;
        for (i = *HEIGHT - 2; i > r - 1; i--) {
            *(TABLE + i + 1) = *(TABLE + i);
        }

		//resizing the height of the array
        *(TABLE + r) = ROW;
        *HEIGHT = *HEIGHT + 1;
    }
}

//inserting the row after the existing row
void IAR (int** TABLE, int* HEIGHT, int* WIDTH, int r) {
    int w = 0; //reading the width of the row
    scanf("%i", &w);
    if (w + 1> 0) {
		//resizing the maximum width
        if (w + 1 > *WIDTH)
            *WIDTH = w + 1;

		//declaring the row array, with one more element for the width size
        int *ROW = malloc((w + 1) * sizeof(int));

		//reading the input and writing in into the array
        int x, c;
        *ROW = w;
        for (x = 1; x < w + 1; x++) {
            scanf("%i", &c);
            *(ROW + x) = c;
        }

		//moving the rows above the row index that we want to insert a new one
        int i = 0;
        for (i = *HEIGHT - 2; i > r; i--) {
            *(TABLE + i + 1) = *(TABLE + i);
        }

		//resizing the height of the array
        *(TABLE + r + 1) = ROW;
        *HEIGHT = *HEIGHT + 1;
    }
}

//swaping two rows
void SWR (int** TABLE, int* HEIGHT, int* WIDTH) {
    int r = 0; //reading the rows coordinates to swap
    int s = 0;
    scanf("%i", &r);
    scanf("%i", &s);

    if ((r < *HEIGHT-1)&&(s < *HEIGHT-1)) {
        //saving the r in temp
        int* temp = *(TABLE + r);
        //saving the s in r place
        *(TABLE + r) = *(TABLE + s);
        //saving the temp in s place
        *(TABLE + s) = temp;
    }
}

//delete the first row
void DFR (int** TABLE, int* HEIGHT, int* WIDTH) {
    if (*HEIGHT-1 > 0) {
        int i = 0;
		//move all rows one step below and reduce the height
        for (i = 0; i + 1 < *HEIGHT; i++) {
            *(TABLE + i) = *(TABLE + i + 1);
        }
        *HEIGHT = *HEIGHT - 1;
    }
}

//remove the given row
void RMR (int** TABLE, int* HEIGHT, int* WIDTH, int r) {
    if ((*HEIGHT-1 > 0)&&(r < *HEIGHT-1))
    {
		//move all rows above that row below and reduce the height
        int i;
        for (i = r; i < *HEIGHT - 1; i++) {
            *(TABLE + i) = *(TABLE + i + 1);
        }
        *HEIGHT = *HEIGHT - 1;
    }
}

//delete first column
void DFC(int **TABLE, int *HEIGHT_MAX, int *WIDTH_MAX, int h_it, int curr_width) {
    //if the row has at least one column
    if (curr_width > 1)
    {
        //move every column from the end one step before
        int i;
        for (i = 1; i < curr_width + 1; i++)
        {
            *(*(TABLE + h_it) + i - 1) = *(*(TABLE + h_it) + i);
        }
        //make the column width smaller
        *(*(TABLE + h_it)) = curr_width - 1;
    }
    //delete the row
    else
    {
        //making it unvisible
        *(*(TABLE + h_it)) = 0;

        RMR(TABLE, HEIGHT_MAX, WIDTH_MAX, h_it);
    }
}
