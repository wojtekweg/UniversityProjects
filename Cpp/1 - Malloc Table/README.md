# malloc() Table

## Task

Write the program that will support the 2D array operations, using **only** dynamic allocated arrays based on malloc() function. The elements of the array are only the int numbers and the width of the each row may vary. 

Usage of ']' or '[' is forbidden, as well as usage of global variables or structs.

## Commands examples
Indexing of the rows and columns starts by 0.

- PRT

Print the current state of the board

- END

Exit the program, free the memory

- AFR 3 4 5 6 

Add the row at the beginning* ot the table with 3 values: 4, 5 and 6 *(ALR, for the end of table)

- AFC 2 8 9

Add the 2 elements column at the beginning* of the table. If height of the input is bigger than the array, create a new rows. If less, then add the columns only in the first (in this case) 2 columns *(ALC, for the end of table)

- IBR 1 2 0 0

Before* the second (count of the rows starts from 0) row insert the 2 elements row with values 0, 0 *(IAR, after the second row)

- IBC x 2 0 0

In the first 2 rows add the 0 and 0 values before* the x column. *(IAC, after the x column)
 
- SWR 0 1

Swap the first and the second (0 and 1) rows* *(SWC, swap the columns)

- DFR / DFC / DLR / DLC

Delete the: first row / first column / last row / last column

- RMR 2

Remove the third row* *(RMC, remove the third column)

- ISB 1 1 2 2 1 2 3 4

After the first row and first column coordinate insert the 2x2 board with 1, 2, 3 and 4 as values. Add the new rows if needed

- RMB 3 3 2 2

Remove the 2x2 sub-board starting from row 3 and column 3
