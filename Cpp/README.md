# Programs list

### Advanced programs

**1. malloc() Table** [C]
  - Simulation of 2D board, with variable number of columns in each row, based on C dynamic allocated arrays
  
**2. Determinent 3D** [C++]
  - Simulation of 3D table, where it's possible to count the determinent of the 2D wall in selected depth. It's also possible to count the volume of cuboid or octal, based in that cube.

### Simple programs

**3. River Simulation** [C++]
  - Based on randomness, program simulates the ecosystem of two animals - bear and fish - where animals can move, each other or reproduce.
  
**4. Logical operations** [C++]
  - Simulation of basic logical set operations (like union, conjuction etc)
  
**5. String Formater** [C++]
  - Format the inputed test - normalization, reading the lexicographical bigger word or word formating included. 
  
**6. Sorting Comparsion** [C++]
  - Compare the merge and quick sort algorithms with the console read command and file reading
