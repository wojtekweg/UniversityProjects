# River Simulation
## Task
Write a program to simulate a river ecosystem (as an array) filled with two animals - bears and fish. Every cell of the river shoul be occupied by bear, fish or NULL. Every animal must have the gender and strength field. Simulate the river by random choice, where the animal can move to the neighbour cell or not move at all. If one animal encounter another, it may take one of the following actions: eat, be eaten, do nothing and step back or reproduce, depending of the specie and gender of each of animals. Additionally, I added the the strength reduction of animals in each iteration.
There is no input data. Visualization of the river is written out in the console.
