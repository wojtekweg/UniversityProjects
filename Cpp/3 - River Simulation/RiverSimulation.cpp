#include <cstdlib>
#include <iostream>
#include <ctime>
/*
 * Wojciech Wegrzyn
 * 2020
 *
 * TRESC ZADANIA
 *
 * Napisz program do symulacji ekosystemu zawierającego dwa rodzaje stworzeń: niedźwiedzi i ryb.
 * Ekosystem składa się z rzeki, która jest modelowana jako stosunkowo duża tablica.
 * Każda komórka tablicy powinna zawierać obiekt zwierzęcy, który może być obiektem niedźwiedzia, obiektem ryby lub NULLem.
 * Dodaj do każdego obiektu zwierzęcego boole'owskie pole mówiące o płci i zmiennoprzecinkowe pole mówiące o sile zwierzaka.
 * W każdym kroku, w oparciu o proces losowy, każde zwierzę próbuje albo przenieść się do sąsiedniej komórki tablicy, albo pozostać tam, gdzie się znajduje.
 * Jeśli niedźwiedź i ryba spotkają się w tej samej komórce, wtedy ryba zostaje zjedzona (czyli znika).
 * Jeśli zderzają się dwa zwierzęta tego samego typu, ale są różnej "płci" to pozostają tam, gdzie są,
 * ale tworzą nową instancję tego typu zwierzęcia, która jest umieszczona w losowo pustej (NULL) komórce w tablicy.
 * W przeciwnym razie, jeśli dwa zwierzęta tego samego typu i tej samej płci próbują się zderzyć, wtedy przetrwa tylko jedno o większej sile.
 * Użyj rzeczywistego tworzenia obiektów, za pomocą nowego operatora, do modelowania tworzenia nowych obiektów i wizualizacji tablicy po każdym kroku.
 * (tego warunku ostatniego zdaje sie, ze mi sie nie udalo zreazlizowac, bo najpierw zle zrozumialem te uwage, a jak odkrylem,
 * ze ja zle rozumiem, to juz byla niedziela wiecozrem i nie zdaze poprawic. ale jak to jest wazne i istenieje taka mozliwosc,
 * to moge to poprawic pozniej)
 *
 * DODATKOWO
 *
 * Program dodatkowo po przejsciu calej rzeki i podjeciu decyzji przez kazde zwierze zmniejsza sile zwierzecia, a jesli jego sila wynosi zero, to go usmierca.
 * W 91. linijce jest zmienna, ktora okresla jak bardzo maleje zdrowie kazdego zwierzaka w kazdej iteracji programu. Fajnie jest sie tym pobawic i obserwowac,
 * jak rozne wartosci wplywaja na przezywalnosc zwierzakow. Niemniej przy bardzo duzych tablicach ta rzeki to dziala troche slabo, bo iteracji sa tysiace,
 * wiec trzeba byloby ustawic te zmienna na jakas 0.00001 czy cos.
 *
 */

class animal {

public:
    int specie; //0 is NULL, 1 is fish, 2 is bear
    bool gender; //0 is M, 1 is F
    float strength; //0.00 - 1.00
};

void print_cell (animal* tab, int i) {
    if (tab[i].specie == 1)
        std::cout << " fish: strength: " << tab[i].strength;
    else if (tab[i].specie == 2)
        std::cout << " bear: strength: " << tab[i].strength;
    else
        std::cout << " ~~~~~~~~~~~~~~~~~~~~~~~~";

    if ((tab[i].specie == 1) || (tab[i].specie == 2)) {
        if (tab[i].gender == 0)
            std::cout <<  " [M]";
        else if (tab[i].gender == 1)
            std::cout <<  " [F]";
    }

    std::cout << " #" << i << std::endl;
}

std::string animal_specie_print (animal* tab, int i) {
    if (tab[i].specie == 0)
        return "NULL"; //std::cout << "NULL";
    else if (tab[i].specie == 1)
        return "fish"; //std::cout << "fish";
    else if (tab[i].specie == 2)
        return "bear"; //std::cout << "bear";
    else
        return "?!";
}

bool winner (animal* tab, int river_size) {
    for (int i = 1; i < river_size; i++) {
        if (tab[i].specie != tab[i - 1].specie) return false;
    }
    return tab[0].specie == tab[1].specie;
}

int bin_search (animal* tab, int strt, int end, int value)
    {
        if (end >= strt)
        {
            int mid = strt + (end - strt) / 2;
            if (tab[mid].specie == value)
                return mid;
            if (tab[mid].specie > value)
                return bin_search(tab, strt, mid - 1, value);
            return bin_search(tab, mid + 1, end, value);
        }
        return -1;
    }

int main() {
    bool river_state_printing = true;
    bool events_printing = true;

    int river_size = 20;
    float strength_reduction = 0.005; //IT'S VERY IMPORTANT TO CHOOSE THAT VALUE WISELY
    animal river[river_size];

    srand((unsigned)time(nullptr));
    //randomizing the river
    for (int i = 0; i < river_size; i++)
    {
        river[i].specie = rand() % 3;
        river[i].gender = rand() % 2;
        river[i].strength = (rand() % 100);
        river[i].strength = river[i].strength/100;

        //printing the river
        if (river_state_printing) print_cell(river, i);
    }

    int iteration = 0;
    if (winner(river, river_size)) std::cout << "whole river has been dominated by " << river[1].specie;
    //program runs, until whole river is filled with one specie or no specie
    while (!winner(river, river_size))
    {
        iteration++;

        //we take action for every specie in river
        //of course if it is specie at all
        for (int i = 0; (i < river_size)&&(!winner(river, river_size)); i++)
        {
            if (river[i].specie != 0)
            {
                //we choose where our specie moves
                int movement = rand() % 3;
                movement--; //making sure that the movement is either -1, 0 or 1 [back, stay, forward]

                //we move our specie along with the randomized movement
                if (movement != 0)
                {
                    //printing current state of river
                    if (river_state_printing)
                    {
                        std:: cout << std::endl;
                        for (int j = 0; j < river_size; j++)
                            print_cell(river, j);
                    }

                    //we choose the neighbour
                    //and we make sure that the neighbour is in river bound
                    //river is cyclic, so there is no way of getting out of river
                    int neighbour = i + movement;
                    if (neighbour < 0)
                        neighbour = river_size - 1;
                    else if (neighbour >= river_size)
                        neighbour = 0;

                    if ((events_printing) && (river[neighbour].specie != 0)) std::cout << "   animal from " << i << " cell, " << animal_specie_print(river, i) << " met animal from " << neighbour << " cell, " << animal_specie_print(river, neighbour) << std::endl;
                    if ((events_printing) && (river[neighbour].specie == 0)) std::cout << "   animal from " << i << " cell, " << animal_specie_print(river, i) << " moved to " << neighbour << " cell" << std::endl;

                    //bear met fish or any of them get into empty cell
                    if (river[i].specie > river[neighbour].specie)
                    {
                        if ((events_printing) && (river[neighbour].specie != 0)) std::cout << "        " << animal_specie_print(river, i) << " ate the " << animal_specie_print(river, neighbour) << std::endl;
                        river[neighbour].specie = river[i].specie;
                        river[neighbour].gender = river[i].gender;
                        river[neighbour].strength = river[i].strength;
                        river[i].specie = 0;
                    }
                    else if (river[i].specie < river[neighbour].specie)
                    {
                        if (events_printing) std::cout << "        " << animal_specie_print(river, i) << " has been eaten by " << animal_specie_print(river, neighbour) << std::endl;
                        river[i].specie = 0;
                    }

                    //two animals of the same specie met
                    else if (river[i].specie == river[neighbour].specie)
                    {
                        if (events_printing) std::cout << "        " << "two animals of " << animal_specie_print(river, i) << " has met" << std::endl;

                        //they are different gender so they make new baby
                        if (river[i].gender != river[neighbour].gender)
                        {
                            //placing the baby at the first empty cell
                            int null_index = bin_search(river, 0, river_size, 0);

                            if (null_index != -1)
                            {
                                //new specie randomization
                                river[null_index].specie = river[i].specie;
                                river[null_index].gender = rand() % 2;
                                river[null_index].strength = (rand() % 50); //because its child xd
                                river[null_index].strength = river[null_index].strength / 100;

                                if (events_printing) std::cout << "            they generated the baby: ";
                                if (events_printing) print_cell(river, null_index);
                            }

                            else if (events_printing) std::cout << "            there was no place for new baby :(" << std::endl;
                        }

                        //they are the same gender so they fight
                        else {
                            if (river[i].strength > river[neighbour].strength)
                            {
                                if (events_printing) std::cout << "   " << animal_specie_print(river, neighbour) << " from " << neighbour << " cell ate its own specie from " << i << " cell" << std::endl;
                                river[neighbour].specie = river[i].specie;
                                river[neighbour].gender = river[i].gender;
                                river[neighbour].strength = river[i].strength;
                                river[i].specie = 0;
                            }
                            else if (river[i].strength < river[neighbour].strength)
                            {
                                if (events_printing) std::cout << "   " << animal_specie_print(river, i) << " from " << i << " cell ate its own specie from " << neighbour << " cell" << std::endl;
                                river[i].specie = 0;
                            }
                            else
                                if (events_printing) std::cout << "   they liked each other, so they didnt fight" << std::endl;
                        }
                    }
                }
            }
        }

        //we reduce the strength of each specie by 0.10
        for (int i = 0; i < river_size; i++) {
            river[i].strength -= strength_reduction;
            if (river[i].strength <= 0)
            {
                if ((events_printing) && (river[i].specie > 0)) std::cout << "      " << animal_specie_print(river, i) << " from " << i << " cell has died of strength decrease" << std::endl;
                river[i].specie = 0;
                river[i].strength = 0;
            }
        }

        //printing current state of river
        if (river_state_printing) {
            std::cout << std::endl;
            std:: cout << "         OUR RIVER AFTER #" << iteration << " ITERATION" << std::endl << std::endl;
            for (int i = 0; i < river_size; i++) {
                std::cout << "~~";
                print_cell(river, i);
            }
            std::cout << std::endl;
        }
    }

    std::cout << std::endl;
    std:: cout << "         PROGRAM ENDED AFTER #" << iteration << " ITERATION" << std::endl << std::endl;
    for (int i = 0; i < river_size; i++) {
        std::cout << "~~";
        print_cell(river, i);
    }
    std::cout<< "river was dominated by " << animal_specie_print(river, 0) << std::endl;

    std::cout << std::endl;

    return 0;
}
